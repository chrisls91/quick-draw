# Quick draw game for Game Design course at Mississippi State University
# Using, modifying, and/or redistributing this code
# without permission from its authors is prohibited.
# Authors: Chris Stewart, Tim Walton, Adam Griffin, and Garrett Talley
import pygbutton
# color definitions
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
RED = (255, 0, 0)
LIME = (181, 230, 29)

# Game state definitions
END = 0
TITLE_SCREEN = 1
RUN_GAME = 2
RUN_VERSUS_GAME = 3
CHARACTER_SELECT = 4
CHARACTER_SELECT_VERSUS = 5
HELP_SCREEN = 6
GAMEOVER_SCREEN = 7

# other definitions
PI = 3.141592653
WINSIZEX = 800
WINSIZEY = 600
STICK_V_MULTIPLE = 10

# Player IDs
PLAYER_1 = 1
PLAYER_2 = 2

# Action IDs
NO_ACTION = 0
FIRE_Q = 1
FIRE_W = 2
FIRE_E = 3
FIRE_R = 4
FAKEOUT = 5

# Menu items
MENU_SINGLE_PLAYER = 0
MENU_VERSUS = 1
MENU_HELP = 2
MENU_IMAGE_LUT = ["images/play_btn.bmp",
                  "images/versus.png", "images/help_btn.png"]

# Menu definitions
MENU_ITEM_COUNT = 3
MENU_LEFT = 0
MENU_RIGHT = 1

# Difficulty levels
DIFF_LEVEL_1 = 1
DIFF_LEVEL_2 = 2
DIFF_LEVEL_3 = 3
DIFF_LEVEL_4 = 4
DIFF_LEVEL_5 = 5

# P1 Button prompts
BTN_Q = 0
BTN_W = 1
BTN_E = 2
BTN_R = 3
BUTTON_IMAGE_LUT = [
    "images/q_cloud.png", "images/w_cloud.png", "images/e_cloud.png", "images/r_cloud.png"]

# P2 Button prompts
BTN_L1 = 0
BTN_D = 1
BTN_R1 = 2
BTN_X = 3
CONTROLLER_BTN_IMAGE_LUT = [
	"images/ps_l1.png","images/ps_square.png","images/ps_r1.png","images/ps_x.png"]

# Background images
NUM_BACKGROUNDS = 4
BACKGROUND_LUT = [
"images/theBank.png","images/theDesert.png","images/theTown.png", "theSquare"]

# Sprite dimensions
DRAWTEXT_HEIGHT = 121
DRAWTEXT_WIDTH = 350
