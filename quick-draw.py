# Quick draw game for Game Design course at Mississippi State University
# Using, modifying, and/or redistributing this code
# without permission from its authors is prohibited.
# Authors: Chris Stewart, Tim Walton, Adam Griffin, and Garrett Talley
import pygame
import math
from CoreAPI import *
from sprites import *
from definitions import *
from pygame.locals import *

# Initialize game engine
pygame.init()
# Initialize score
score = 0
# initial fade values
start = 255
end = 0

# Check for game controllers
joystick_count = pygame.joystick.get_count()
if (joystick_count == 0):
    print("no controllers found")
else:
    controller = pygame.joystick.Joystick(0)
    controller.init()

# Load audio
titleShootSound = pygame.mixer.Sound("audio/title_gunshot.wav")
goSound = pygame.mixer.Sound("audio/go.wav")
BangSound = pygame.mixer.Sound("audio/shoot.wav")
selectionToggleSound = pygame.mixer.Sound("audio/boing.wav")
bearGrowl = pygame.mixer.Sound("audio/growl.wav")

# Load music
mainMusic = "music/gameMusic1.wav"
lvlMusic = "music/gameMusic3.mp3"
bossMusic = "music/bossMusic1.mp3"

allMusic = [mainMusic, lvlMusic, bossMusic]

# Set window size
windowSize = (WINSIZEX, WINSIZEY)
screen = pygame.display.set_mode(windowSize)

# Window title
pygame.display.set_caption("Quick Draw")

# Create Sprite Groups
allSprites = pygame.sprite.Group()
playerSprites = pygame.sprite.Group()
visualCueSprites1P = pygame.sprite.Group()
visualCueSprites2P = pygame.sprite.Group()

# Load background image
menuBackground = pygame.image.load("images/menu_background.png")
helpImg = pygame.image.load("images/Keyboard_Help.png")
gameBackground = pygame.image.load(BACKGROUND_LUT[random.randrange(0, 2)])
fade = pygame.image.load("images/fade.png").convert()

# Create UI sprites
drawCue = DrawCue()
btnCueP1 = Player1Prompt()
btnCueP2 = Player2Prompt()
visualCueSprites1P.add(drawCue)
visualCueSprites1P.add(btnCueP1)
visualCueSprites2P.add(drawCue)
visualCueSprites2P.add(btnCueP1)
visualCueSprites2P.add(btnCueP2)

clock = pygame.time.Clock()

soundPlayed = False
fakeSoundPlayed = False

# Create game manager and computer player
manager = GameManager()
compPlayer = ComputerPlayer()

# Menu muscic initialization
music = Music(allMusic)
pygame.mixer.init()

# Instantiate button objects for main menu
playButton = pygbutton.PygButton((170, 300, 200, 30), normal='images/play_btn.png',
                                 down='images/play_btn.png', highlight='images/play_btn_highlight.png')
versusButton = pygbutton.PygButton(
    (340, 280, 200, 30), normal='images/versus.png', down='images/versus.png', highlight='images/versus_highlight.png')
helpButton = pygbutton.PygButton((510, 260, 200, 30), normal='images/help_btn.png',
                                 down='images/help_btn.png', highlight='images/help_btn_highlight.png')
bearButton = pygbutton.PygButton((200, 270, 200, 30), normal='images/Bearheart_portrait.png',
                                 down='images/Bearheart_portrait.png', highlight='images/Bearheart_highlight.png')
backButton = pygbutton.PygButton((0, 0, 0, 0), normal='images/back.png',
                                 down='images/back.png', highlight='images/back_highlight.png')
ramboButton = pygbutton.PygButton((300, 270, 300, 30), normal='images/Rambro_dark.png',
                                  down='images/Rambro_dark.png', highlight='images/Rambro_portrait.png')

gameState = TITLE_SCREEN

music.update(gameState, manager.diffLevel)
# Main loop
while(gameState != END):
    # Process input events.
    for event in pygame.event.get():
        # If user clicks close button, terminate
        if(event.type == pygame.QUIT)or (event.type == KEYDOWN and event.key == K_ESCAPE):
            print("user quit")
            gameState = END

        playBtnEvents = playButton.handleEvent(event)
        helpBtnEvents = helpButton.handleEvent(event)
        versusBtnEvents = versusButton.handleEvent(event)
        bearBtnEvents = bearButton.handleEvent(event)
        backBtnEvents = backButton.handleEvent(event)
        ramboBtnEvents = ramboButton.handleEvent(event)

        # If user presses enter, start
        # TODO: start correct game mode based on menu selection
        if('click' in playBtnEvents and gameState == TITLE_SCREEN):
            gameState = CHARACTER_SELECT
            manager.start()

        elif('click' in bearBtnEvents and (gameState == CHARACTER_SELECT or gameState == CHARACTER_SELECT_VERSUS)):
            # initiate player sprite
            player1 = BearheartP1()
            playerSprites = pygame.sprite.Group()
            playerSprites.add(player1)
            player2 = RamboP2()
            playerSprites.add(player2)

            playerSound = bearGrowl

            # Get the end time from the game manager so the numbers are the
            # same and set it for the reaction Time
            if(gameState == CHARACTER_SELECT):
                gameState = RUN_GAME
                endTime = manager.start()
                compPlayer.endTime = endTime
                compPlayer.start(DIFF_LEVEL_1)
                compPlayer.actionButton = manager.player2Button + 1
                btnCueP1.setImage(manager.player1Button)
                btnCueP2.setImage(manager.player2Button)
                gameBackground = pygame.image.load(
                    BACKGROUND_LUT[random.randrange(0, 2)])

            if(gameState == CHARACTER_SELECT_VERSUS):
                gameState = RUN_VERSUS_GAME
                endTime = manager.start()
                btnCueP1.setImage(manager.player1Button)
                btnCueP2.setImage(manager.player2Button)
                gameBackground = pygame.image.load(
                    BACKGROUND_LUT[random.randrange(0, 2)])

        elif('click' in ramboBtnEvents and (gameState == CHARACTER_SELECT or gameState == CHARACTER_SELECT_VERSUS)):
            # initiate player sprite
            player1 = RamboP1()
            playerSprites = pygame.sprite.Group()
            playerSprites.add(player1)
            player2 = BearheartP2()
            playerSprites.add(player2)

            playerSound = BangSound

            # Get the end time from the game manager so the numbers are the
            # same and set it for the reaction Time
            if(gameState == CHARACTER_SELECT):
                gameState = RUN_GAME
                endTime = manager.start()
                compPlayer.endTime = endTime
                compPlayer.start(DIFF_LEVEL_1)
                compPlayer.actionButton = manager.player2Button + 1
                btnCueP1.setImage(manager.player1Button)
                btnCueP2.setImage(manager.player2Button)
                gameBackground = pygame.image.load(
                    BACKGROUND_LUT[random.randrange(0, 2)])

            elif(gameState == CHARACTER_SELECT_VERSUS):
                gameState = RUN_VERSUS_GAME
                endTime = manager.start()
                btnCueP1.setImage(manager.player1Button)
                btnCueP2.setImage(manager.player2Button)
                gameBackground = pygame.image.load(
                    BACKGROUND_LUT[random.randrange(0, 2)])

        if('click' in backBtnEvents and (gameState == CHARACTER_SELECT or gameState == HELP_SCREEN)):
            gameState = TITLE_SCREEN
            manager.start()

        if('click' in helpBtnEvents and gameState == TITLE_SCREEN):
            gameState = HELP_SCREEN
            manager.start()

        if('click' in versusBtnEvents and gameState == TITLE_SCREEN):
            gameState = CHARACTER_SELECT_VERSUS
            manager.start()

        if(event.type == pygame.KEYDOWN):
            # If game is running, handle action button
            if(event.key == pygame.K_q):
                if(manager.isRunning):
                    manager.reportPlayerAction(PLAYER_1, FIRE_Q)
            elif(event.key == pygame.K_w):
                if(manager.isRunning):
                    manager.reportPlayerAction(PLAYER_1, FIRE_W)
            elif(event.key == pygame.K_e):
                if(manager.isRunning):
                    manager.reportPlayerAction(PLAYER_1, FIRE_E)
            elif(event.key == pygame.K_r):
                if(manager.isRunning):
                    manager.reportPlayerAction(PLAYER_1, FIRE_R)
            elif(event.key == pygame.K_SPACE):
                if(manager.isRunning):
                    manager.reportPlayerAction(PLAYER_1, FAKEOUT)

        # If two-player game, handle player 2 action
        if(event.type == pygame.JOYBUTTONDOWN):
            print(event.button)
            if(event.button == 4):
                if(manager.isRunning and gameState == RUN_VERSUS_GAME):
                    manager.reportPlayerAction(PLAYER_2, FIRE_Q)
            if(event.button == 0):
                if(manager.isRunning and gameState == RUN_VERSUS_GAME):
                    manager.reportPlayerAction(PLAYER_2, FIRE_W)
            if(event.button == 5):
                if(manager.isRunning and gameState == RUN_VERSUS_GAME):
                    manager.reportPlayerAction(PLAYER_2, FIRE_E)
            if(event.button == 1):
                if(manager.isRunning and gameState == RUN_VERSUS_GAME):
                    manager.reportPlayerAction(PLAYER_2, FIRE_R)
            if(event.button == 7):
                if(manager.isRunning and gameState == RUN_VERSUS_GAME):
                    manager.reportPlayerAction(PLAYER_2, FAKEOUT)

    # Title screen state
    if(gameState == TITLE_SCREEN):
        # Draw background
        screen.blit(menuBackground, [0, 0])
        playButton.draw(screen)
        helpButton.draw(screen)
        versusButton.draw(screen)
        pygame.display.update()
        # Set the number of lives and the circle for it
        lives = 1
    if(gameState == GAMEOVER_SCREEN):
        #Game Over
        screen.fill((0,0,0))
        scoreString = "Your Score was: " + str(scoreTrunc)
        font = pygame.font.Font(None, 46)
        text = font.render(scoreString, 1, (255, 255, 255), None)
        screen.blit(text, ((screen.get_width()/2) - 150, screen.get_height()/2))

        gameOverString = "Game Over"
        font = pygame.font.Font(None, 100)
        text = font.render(gameOverString, 1, (255, 255, 255), None)
        screen.blit(text, ((screen.get_width()/2) - 200, (screen.get_height()/2) - 80))

        if(event.type == pygame.KEYDOWN):
            gameState = TITLE_SCREEN
            print("TEST")

    if(gameState == HELP_SCREEN):
        # Draw background
        screen.blit(helpImg, [0, 0])
        backButton.draw(screen)
        pygame.display.update()

    # Character Select screen
    elif(gameState == CHARACTER_SELECT or gameState == CHARACTER_SELECT_VERSUS):
        # Draw background
        screen.blit(menuBackground, [0, 0])
        backButton.draw(screen)
        bearButton.draw(screen)
        ramboButton.draw(screen)
        pygame.display.update()

    # Game running state
    elif(gameState == RUN_GAME):
        # Fill screen with white
        screen.blit(gameBackground, [0, 0])

        # Turn the score into a string so it can be displayed
        scoreTrunc = math.trunc(score)
        scoreString = "Score: " + str(scoreTrunc)
        # Display the score
        font = pygame.font.Font(None, 36)
        text = font.render(scoreString, 1, (0, 0, 0), None)
        screen.blit(text, (0, 0))

        # Draw the circle on the screen. The last 0 is the width which fills.
        if(lives):
            pygame.draw.circle(screen, BLACK, (93, 588), 10, 0)
        font = pygame.font.Font(None, 36)
        text = font.render("Lives: ", 1, (0, 0, 0), None)
        # Display the text "Lives:"
        screen.blit(text, (3, 575))

        # Update and draw sprites
        playerSprites.update()
        playerSprites.draw(screen)
        # When the game is triggered, notify players
        if(manager.isTriggered):
            drawCue.setRealImage()
            if(soundPlayed is False):
                goSound.play()
                drawCue.runEntryAnimation()
                soundPlayed = True
            visualCueSprites1P.update()
            visualCueSprites1P.draw(screen)

        # If a fakeout is happening, display cues
        elif(manager.fakeout):
            drawCue.setFakeImage()
            if(soundPlayed is False):
                goSound.play()
                drawCue.runEntryAnimation()
                soundPlayed = True
            visualCueSprites1P.update()
            visualCueSprites1P.draw(screen)

        else:
            soundPlayed = False

        # Process computer player
        if(manager.isTriggered):
            compPlayer.trigger()
        compAction = compPlayer.update()
        manager.reportPlayerAction(PLAYER_2, compAction)

        # If player 1 has won the game, display appropriate animation and
        # play sound
        if(manager.winner == PLAYER_1):
            # start black screen fade sequence
            player2.animationLoseStart()
            player1.animationWinStart()
            screen.blit(fade, [0, 0])
            if(start == 255):
                # update display for pause
                pygame.display.flip()
                titleShootSound.play()
            if(start > 0):
                fade.set_alpha(start)
                start -= 1

            # display text
            # font = pygame.font.SysFont('Calibri', 40, True, False)
            # text = font.render("Player 1 Wins!", True, BLACK)
            # screen.blit(text,[400,300])

            # If animation has not started
            if(player2.loseTimer == 0):
                player2.runLoseAnimation()
                playerSound.play()

            # Check for animation completion
            elif(player2.animationIsComplete()):
                # Stop game manager
                start = 255
                score = manager.stop()
                compPlayer.stop()

                # Reset sprites and go back to title screen state
                player1.resetPosition()
                player2.resetPosition()
                soundPlayed = False

                # When 5 levels are complete
                if(compPlayer.diffLevel >= DIFF_LEVEL_5):
                    gameState = GAMEOVER_SCREEN

                else:
                    # Get the end time from the game manager so the numbers
                    # are the same and set it for the reaction Time
                    endTime = manager.start()
                    compPlayer.endTime = endTime
                    compPlayer.start(compPlayer.diffLevel + 1)
                    manager.diffLevel = compPlayer.diffLevel
                    compPlayer.actionButton = manager.player2Button + 1
                    btnCueP1.setImage(manager.player1Button)
                    btnCueP2.setImage(manager.player2Button)
                    gameBackground = pygame.image.load(
                        BACKGROUND_LUT[random.randrange(0, 2)])

        elif(manager.winner == PLAYER_2):

            # start black screen fade sequence
            player1.animationLoseStart()
            player2.animationWinStart()
            screen.blit(fade, [0, 0])
            if(start == 255):
                # update display for pause
                pygame.display.flip()
                titleShootSound.play()
            if(start > 0):
                fade.set_alpha(start)
                start -= 1

            # If animation has not started
            elif(player1.loseTimer == 0):
                player1.runLoseAnimation()
                BangSound.play()

            # Check for animation completion
            elif(player1.animationIsComplete()):
                if(lives):
                    # Subract the lives. Lives currently set to 1 originally.
                    lives -= 1
                    # Stop game manager
                    start = 255
                    manager.stop()
                    compPlayer.stop()
                    # Reset sprites
                    player1.resetPosition()
                    player2.resetPosition()
                    soundPlayed = False
                    # Code to cover the circle. Need to move. Code taken from here
                    # http://stackoverflow.com/questions/9286738/pygame-erasing-images-on-backgrounds
                    # It places the gameBackground in a rectangle size 30x30 at
                    # 83x and 578y
                    screen.blit(
                        gameBackground, (83, 578), pygame.Rect(83, 578, 30, 30))
                    # Same code as if you had won except it doesn't change the background
                    # Or increase the difficulty of the computer player.
                    endTime = manager.start()
                    compPlayer.endTime = endTime
                    compPlayer.start(compPlayer.diffLevel)
                    manager.diffLevel = compPlayer.diffLevel
                    compPlayer.actionButton = manager.player2Button + 1
                    btnCueP1.setImage(manager.player1Button)
                    btnCueP2.setImage(manager.player2Button)

                else:
                    # Stop game manager
                    start = 255
                    manager.stop()
                    compPlayer.stop()
                    # Reset the score to 0
                    manager.score = 0
                    score = 0
                    # Reset sprites and go back to title screen state
                    # Tim Walton: I commented out the below two lines as they were redrawing sprites after the game
                    # ended, if I broke some necessiary functionality just uncomment them but sprites being drawn
                    # over other sprites would need to be fixed
                    # player1.resetPosition()
                    # player2.resetPosition()
                    soundPlayed = False

                    gameState = GAMEOVER_SCREEN

        # Update manager
        manager.update()

    # 2 player game running state.
    elif(gameState == RUN_VERSUS_GAME):
        # Fill screen with white
        screen.blit(gameBackground, [0, 0])

        # Update and draw sprites
        playerSprites.update()
        playerSprites.draw(screen)

        # When the game is triggered, notify players
        if(manager.isTriggered):
            drawCue.setRealImage()
            if(soundPlayed is False):
                goSound.play()
                drawCue.runEntryAnimation()
                soundPlayed = True
            visualCueSprites2P.update()
            visualCueSprites2P.draw(screen)

        # If a fakeout is happening, display cues
        if(manager.fakeout):
            drawCue.setFakeImage()
            if(soundPlayed is False):
                goSound.play()
                drawCue.runEntryAnimation()
                soundPlayed = True
            visualCueSprites2P.update()
            visualCueSprites2P.draw(screen)

        # If player 1 has won the game, display appropriate animation and
        # play sound
        if(manager.winner == PLAYER_1):

            # start black screen fade sequence
            player2.animationLoseStart()
            player1.animationWinStart()
            screen.blit(fade, [0, 0])
            if(start == 255):
                # update display for pause
                pygame.display.flip()
                titleShootSound.play()
            if(start > 0):
                fade.set_alpha(start)
                start -= 1

            # If animation has not started
            elif(player2.loseTimer == 0):
                player2.runLoseAnimation()
                playerSound.play()

            # Check for animation completion
            elif(player2.animationIsComplete()):
                # Stop game manager
                start = 255
                manager.stop()
                compPlayer.stop()

                # Reset sprites and go back to title screen state
                player1.resetPosition()
                player2.resetPosition()
                soundPlayed = False

                gameState = TITLE_SCREEN

        elif(manager.winner == PLAYER_2):

            # start black screen fade sequence
            player1.animationLoseStart()
            player2.animationWinStart()
            screen.blit(fade, [0, 0])
            if(start == 255):
                # update display for pause
                pygame.display.flip()
                titleShootSound.play()
            if(start > 0):
                fade.set_alpha(start)
                start -= 1

            # If animation has not started
            elif(player1.loseTimer == 0):
                player1.runLoseAnimation()
                BangSound.play()

            # Check for animation completion
            elif(player1.animationIsComplete()):
                # Stop game manager
                start = 255
                manager.stop()
                compPlayer.stop()

                # Reset sprites and go back to title screen state
                player1.resetPosition()
                player2.resetPosition()
                soundPlayed = False

                gameState = TITLE_SCREEN

        # Update manager
        manager.update()

    # Update music
    music.update(gameState, manager.diffLevel)

    # Refresh at 60hz
    clock.tick(60)

    pygame.display.flip()

# Kill pygame
pygame.quit()
