# Quick draw game for Game Design course at Mississippi State University
# Using, modifying, and/or redistributing this code
# without permission from its authors is prohibited.
# Authors: Chris Stewart, Tim Walton, Adam Griffin, Scott Stephan, and Garrett Talley
import pygame
import random
import time
from definitions import *


# Simple timer class for counting down
# to start of shootoff. Doesn't keep time in any
# meaningful format, but we don't need it to
# for our purposes
# TODO: Adjust SimpleTimer as appropriate for gameplay
class SimpleTimer:

    def __init__(self):
        self.ticks = 0
        self.chunks = 0
        self.elapsed = 0
        # Start the python Timer
        self.start = time.time()

    def tick(self):
        if(self.ticks >= 50):
            self.ticks = 0
            self.chunks += 1
        self.ticks += 1

    def clear(self):
        self.ticks = 0
        self.chunks = 0
        self.elapsed = 0

    # New function for the python timer
    def pyTimer(self):
        # Idea for the code taken here.
        # http://codereview.stackexchange.com/questions/26534/is-there-a-better-way-to-count-seconds-in-python

        time.clock()
        elapsed = time.time() - self.start
        self.elapsed = elapsed
        # print(elapsed)

    def getValue(self):
        return self.elapsed


# Game Manager class
class GameManager:

    def __init__(self):
        self.isRunning = False
        self.isTriggered = False
        self.fakeout = False
        self.fakeoutTimer = 0
        self.isFinished = False
        self.winner = 0
        self.timer = SimpleTimer()
        self.endTime = 0
        self.fakeoutTime = 0
        self.score = 0
        self.diffLevel = 0

        self.player1Button = 0
        self.player1FakeoutUsed = False
        self.player2Button = 0
        self.player2FakeoutUsed = False

    def start(self):
        self.isRunning = True
        # Range changed from 2 - 10. Felt 1 wasn't long enough
        self.endTime = random.randrange(3, 10)
        self.fakeoutTime = random.randrange(1, self.endTime-1)
        #'Clear'the timer by using this as the last called one
        self.timer.start = time.time()
        # return endTime to be used for reaction time

        self.player1Button = random.randrange(0,3)
        self.player2Button = random.randrange(0,3)
        return self.endTime

    def stop(self):
        if(self.winner==PLAYER_1):
            score = (10-(self.timer.getValue() - self.endTime)) * 1000
            self.score += score
            print(score)
            print(self.score)
        else:
            score = 0
        self.isRunning = False
        self.isTriggered = False
        self.isFinished = False
        self.winner = 0
        self.timer.clear()
        self.endtime = 0
        self.player1FakeoutUsed = False
        self.player2FakeoutUsed = False
        return self.score

    def update(self):
        if(self.fakeout is True):
            self.fakeoutTimer+=1
            if(self.fakeoutTimer>=20):
                self.fakeout = False
                self.fakeoutTimer = 0

        elif(self.isRunning and not self.isTriggered):
            self.timer.pyTimer()
            #print(self.diffLevel)
            
            if((self.timer.getValue() >= self.fakeoutTime) and (self.diffLevel > DIFF_LEVEL_3)): 
                self.fakeout = True
                self.fakeoutTime = 100
                
            
            if(self.timer.getValue() >= self.endTime):
                self.isTriggered = True

    def reportPlayerAction(self, playerID, action):
        if(self.isRunning is True and action != NO_ACTION):
            print("hit")
            if(action == FAKEOUT):
                if(playerID == PLAYER_1 and self.player1FakeoutUsed is False):
                    self.player1FakeoutUsed = True
                    self.fakeout = True
                elif(playerID == PLAYER_2 and self.player2FakeoutUsed is False):
                    self.player2FakeoutUsed = True
                    self.fakeout = True

            elif(self.isTriggered is False or 
                (playerID == PLAYER_1 and self.player1Button+1!=action) or
                (playerID == PLAYER_2 and self.player2Button+1!=action)):
                if(playerID == PLAYER_1):
                    self.winner = PLAYER_2
                else:
                    self.winner = PLAYER_1
                self.isFinished = True
                self.isRunning = False
                self.isTriggered = False

            else:
                self.winner = playerID
                self.isFinished = True
                self.isRunning = False
                self.isTriggered = False
    def getDiffLevel(self, diffLevel):
        self.diffLevel = diffLevel

# Class for computer controlled player
class ComputerPlayer:

    def __init__(self):
        self.isRunning = False
        self.isTriggered = False
        self.actionButton = 0
        self.reactionTime = 0
        self.fakeoutTime = 0
        self.diffLevel = 0
        self.timer = SimpleTimer()
        #self.manager = GameManager()
        # Needs the endTime
        self.endTime = 0

    def start(self, diffLevel):
        # TODO: Implement more difficulty levels
        #'Clear'the timer by using this as the last called one
        self.timer.start = time.time()
        self.diffLevel = diffLevel
        self.isRunning = True
        # TODO: Dynamically generate a more random/reasonable
        # reaction time
        # Changed the reaction time to include endTime to use the python
        # Timer
        endTime = self.endTime
        # Reaction time in Seconds the endTime is for the timer so the
        # number that is added is the actual reaction time
        if(diffLevel == DIFF_LEVEL_1):
            self.reactionTime = (1 + endTime)
            print("1")
        elif(diffLevel == DIFF_LEVEL_2):
            self.reactionTime = (.8 + endTime)
            print("2")
        elif(diffLevel == DIFF_LEVEL_3):
            self.reactionTime = (.6 + endTime)
            print("3")
        elif(diffLevel == DIFF_LEVEL_4):
            self.reactionTime = (.4 + endTime)
            print("4")
        elif(diffLevel == DIFF_LEVEL_5):
            self.reactionTime = (.2 + endTime)
            print("5")
        else:
            print("Test")


    def stop(self):
        self.isRunning = False
        self.isTriggered = False
        self.timer.clear()

    def update(self):
        if(self.isRunning and self.isTriggered):
            # Use the pyTimer
            self.timer.pyTimer()
            if(self.timer.getValue() >= self.reactionTime):
                self.isRunning = False
                self.isTriggered = False
                self.timer.clear()
                return self.actionButton
        return 0

    def trigger(self):
        if(self.isRunning):
            self.isTriggered = True


class Music(ComputerPlayer):

    def __init__(self, music):
        self.menu = False
        self.lvl = False
        self.tracks = []
        self.track = 0
        for track in music:
            self.tracks.append(track)
        
    def update(self, state, level):  
        if state == 1 or state == 4 or state == 5:
            if self.lvl == True:
                self.stop()
                self.lvl = False
            self.menu = True
            if not pygame.mixer.music.get_busy():
                pygame.mixer.music.load(self.tracks[0])
                pygame.mixer.music.set_volume(0.5)
                pygame.mixer.music.play(-1)
        if state == 2:
            self.lvl = True
            if level < 5:
                if self.menu == True:
                    self.stop()
                    self.menu = False
                if not pygame.mixer.music.get_busy():
                    self.play()
            if level == 5:
                if self.menu == False:
                    self.stop()
                    self.menu = True
                if not pygame.mixer.music.get_busy():
                    self.play2()

    def play(self):  
        pygame.mixer.music.load(self.tracks[1])
        pygame.mixer.music.set_volume(0.5)
        pygame.mixer.music.play(-1, -2)

    def play2(self):  
        pygame.mixer.music.load(self.tracks[2])
        pygame.mixer.music.set_volume(0.5)
        pygame.mixer.music.play(-1)


    def stop(self):
        pygame.mixer.music.stop()


    


