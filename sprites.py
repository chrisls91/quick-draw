# Quick draw game for Game Design course at Mississippi State University
# Using, modifying, and/or redistributing this code
# without permission from its authors is prohibited.
# Authors: Chris Stewart, Tim Walton, Adam Griffin, and Garrett Talley
import pygame
from definitions import *

class DrawCue(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        self.image = pygame.image.load("images/draw.bmp")

        self.image.set_colorkey(WHITE)

        self.rect = self.image.get_rect()
        self.rect.x = (WINSIZEX / 2 - 150)
        self.rect.y = (WINSIZEY / 2 - 150)

        self.animationCount = 0
        self.animationIsRunning = False

    def setFakeImage(self):
        self.image = pygame.image.load("images/draw_fake.bmp")

    def setRealImage(self):
        self.image = pygame.image.load("images/draw.bmp")

    def runEntryAnimation(self):
        if(self.animationIsRunning is False):
            self.animationIsRunning = True
            self.image = pygame.transform.smoothscale(
                self.image, (DRAWTEXT_WIDTH, 1))

    # Update method that contains animation code for text
    def update(self):
        if(self.animationIsRunning is True):
            if(self.animationCount >= DRAWTEXT_HEIGHT):
                self.animationIsRunning = False
                self.animationCount = 0

            else:
                self.image = pygame.transform.smoothscale(
                    self.image, (DRAWTEXT_WIDTH, self.animationCount))
                self.animationCount+=10


class Player1Prompt(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        self.image = pygame.image.load(BUTTON_IMAGE_LUT[0])

        self.rect = self.image.get_rect()
        self.rect.x = WINSIZEX * .05 + 60
        self.rect.y = WINSIZEY * .50 - 100

    def setImage(self, buttonImage):
        self.image = pygame.image.load(BUTTON_IMAGE_LUT[buttonImage])


class Player2Prompt(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        self.image = pygame.image.load(CONTROLLER_BTN_IMAGE_LUT[0])

        self.rect = self.image.get_rect()
        self.rect.x = WINSIZEX * .80 - 150
        self.rect.y = WINSIZEY * .50 - 100

    def setImage(self, buttonImage):
        self.image = pygame.image.load(CONTROLLER_BTN_IMAGE_LUT[buttonImage])

# Class for BearHeart sprite
class BearheartP1(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        # Initialize losing animation flag
        self.loseAnimation = False
        self.animationComplete = False
        self.loseTimer = 0

        # Load image
        self.image = pygame.image.load("images/BearheartP1.png").convert()

        # Set transparent color
        self.image.set_colorkey(LIME)

        # Set horizontal velocity
        self.xv = 0

        # Set rectangle object
        self.rect = self.image.get_rect()

        # Initialize sprite torwards the left of the screen
        self.rect.x = WINSIZEX * .05
        self.rect.y = (WINSIZEY * .50) + 90

    def update(self):
        if(self.loseAnimation):
            self.loseTimer += 1
            if(self.loseTimer >= 100):
                self.loseAnimation = False
                self.animationComplete = True
                self.loseTimer = 0

    def animationIsRunning(self):
        return self.loseAnimation

    def animationIsComplete(self):
        return self.animationComplete

    def animationLoseStart(self):
        self.image = pygame.image.load("images/bearheart_dead.png").convert()
        self.image.set_colorkey(LIME)

    def animationWinStart(self):
        self.image = pygame.image.load("images/bearheart_strike.png").convert()
        self.rect.x = WINSIZEX * .85
        self.image.set_colorkey(LIME)

    def runLoseAnimation(self):
        self.loseAnimation = True
        self.xv = -20

    def resetPosition(self):
        self.image = pygame.image.load("images/BearheartP1.png").convert()
        self.image.set_colorkey(LIME)
        self.rect.x = WINSIZEX * .05
        self.rect.y = (WINSIZEY * .50) + 90
        self.loseAnimation = False
        self.animationComplete = False
        self.xv = 0
        self.loseTimer = 0

# Class for BearHeart player2 sprite
class BearheartP2(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        # Initialize losing animation flag
        self.loseAnimation = False
        self.animationComplete = False
        self.loseTimer = 0

        # Load image
        self.image = pygame.image.load("images/BearheartP2.png").convert()

        # Set transparent color
        self.image.set_colorkey(LIME)

        # Set horizontal velocity
        self.xv = 0

        # Set rectangle object
        self.rect = self.image.get_rect()

        # Initialize sprite torwards the right of the screen
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 90

    def update(self):
        if(self.loseAnimation):
            self.loseTimer += 1
            if(self.loseTimer >= 200):
                self.loseAnimation = False
                self.animationComplete = True
                self.loseTimer = 0
    def animationIsRunning(self):
        return self.loseAnimation

    def animationIsComplete(self):
        return self.animationComplete

    def animationLoseStart(self):
        self.image = pygame.image.load("images/bearheart_deadP2.png").convert()
        self.image.set_colorkey(LIME)

    def animationWinStart(self):
        self.image = pygame.image.load("images/bearheart_strikeP2.png").convert()
        self.rect.x = WINSIZEX * .05
        self.image.set_colorkey(LIME)
        
    def runLoseAnimation(self):
        self.loseAnimation = True

    def resetPosition(self):
        self.image = pygame.image.load("images/BearheartP2.png").convert()
        self.image.set_colorkey(LIME)
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 90
        self.loseAnimation = False
        self.animationComplete = False
        self.xv = 0
        self.loseTimer = 0

# Class for BearHeart player2 sprite
class RamboP2(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        # Initialize losing animation flag
        self.loseAnimation = False
        self.animationComplete = False
        self.loseTimer = 0

        # Load image
        self.image = pygame.image.load("images/rambroP2.png").convert()

        # Set transparent color
        self.image.set_colorkey(LIME)

        # Set horizontal velocity
        self.xv = 0

        # Set rectangle object
        self.rect = self.image.get_rect()

        # Initialize sprite torwards the right of the screen
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 90

    def update(self):
        if(self.loseAnimation):
            self.loseTimer += 1
            if(self.loseTimer >= 100):
                self.loseAnimation = False
                self.animationComplete = True
                self.loseTimer = 0

    def animationIsRunning(self):
        return self.loseAnimation

    def animationIsComplete(self):
        return self.animationComplete

    def animationLoseStart(self):
        self.image = pygame.image.load("images/rambro_deadP2.png").convert()
        self.image.set_colorkey(LIME)

    def animationWinStart(self):
        self.image = pygame.image.load("images/rambro_strike.png").convert()
        self.image.set_colorkey(LIME)
        
    def runLoseAnimation(self):
        self.loseAnimation = True
        self.xv = 20

    def resetPosition(self):
        self.image = pygame.image.load("images/rambroP2.png").convert()
        self.image.set_colorkey(LIME)
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 90
        self.loseAnimation = False
        self.animationComplete = False
        self.xv = 0
        self.loseTimer = 0

class RamboP1(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        # Initialize losing animation flag
        self.loseAnimation = False
        self.animationComplete = False
        self.loseTimer = 0

        # Load image
        self.image = pygame.image.load("images/rambroP1.png").convert()

        # Set transparent color
        self.image.set_colorkey(LIME)

        # Set horizontal velocity
        self.xv = 0

        # Set rectangle object
        self.rect = self.image.get_rect()

        # Initialize sprite torwards the left of the screen
        self.rect.x = WINSIZEX * .05
        self.rect.y = (WINSIZEY * .50) + 90

    def update(self):
        if(self.loseAnimation):
            self.loseTimer += 1
            if(self.loseTimer >= 100):
                self.loseAnimation = False
                self.animationComplete = True
                self.loseTimer = 0

    def animationIsRunning(self):
        return self.loseAnimation

    def animationLoseStart(self):
        self.image = pygame.image.load("images/rambro_dead.png").convert()
        self.image.set_colorkey(LIME)

    def animationWinStart(self):
        self.image = pygame.image.load("images/rambro_strike.png").convert()
        self.image.set_colorkey(LIME)

    def animationIsComplete(self):
        return self.animationComplete        

    def runLoseAnimation(self):
        self.loseAnimation = True
        self.xv = -20

    def resetPosition(self):
        self.image = pygame.image.load("images/rambroP1.png").convert()
        self.image.set_colorkey(LIME)
        self.rect.x = WINSIZEX * .05
        self.rect.y = (WINSIZEY * .50) + 90
        self.loseAnimation = False
        self.animationComplete = False
        self.xv = 0
        self.loseTimer = 0


# Class for Player2 sprite
class Player2(pygame.sprite.Sprite):

    def __init__(self):
        super().__init__()

        # Initialize losing animation flag
        self.loseAnimation = False
        self.animationComplete = False

        # Load image
        self.image = pygame.image.load("images/rambroP2.png").convert()

        # Set transparent color
        self.image.set_colorkey(LIME)

        # Set horizontal velocity
        self.xv = 0

        # Set rectangle object
        self.rect = self.image.get_rect()

        # Initialize sprite torwards the right of the screen
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 70

    def update(self):
        if(self.loseAnimation):
            self.rect.x += self.xv

            if(self.rect.x >= WINSIZEX + 400):
                self.loseAnimation = False
                self.animationComplete = True

    def animationIsRunning(self):
        return self.loseAnimation

    def animationIsComplete(self):
        return self.animationComplete

    def animationLoseStart(self):
        self.image = pygame.image.load("images/rambro_deadP2.png").convert()
        self.image.set_colorkey(LIME)

    def animationWinStart(self):
        self.image = pygame.image.load("images/rambro_strike.png").convert()
        self.image.set_colorkey(LIME)
        
    def runLoseAnimation(self):
        self.loseAnimation = True
        self.xv = 20

    def resetPosition(self):
        self.image = pygame.image.load("images/rambroP2.png").convert()
        self.image.set_colorkey(LIME)
        self.rect.x = WINSIZEX * .80
        self.rect.y = (WINSIZEY * .50) + 70
        self.loseAnimation = False
        self.animationComplete = False
        self.xv = 0
